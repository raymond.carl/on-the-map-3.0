//
//  LoginCredentials.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 22/11/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation

class LoginCredentials {

    static let shared = LoginCredentials(registered: <#Bool#>, key: <#String#>, expiration: <#String#>, id: <#String#>)
    
    let account: Account
    let session: Session
    let credentials: RequestLoginResponse
    
    private init(registered: Bool, key: String, expiration: String, id: String) {
        self.account = Account(registered: registered, key: key)
        self.session = Session(expiration: expiration, id: id)
        self.credentials = RequestLoginResponse(account: account, session: session)
        //self.credentials = RequestLoginResponse(account: account, session: session)
    }
    
    struct RequestLoginResponse: Codable {
        let account: Account
        let session: Session
        
        enum CodingKeys: String, CodingKey {
            case account = "account"
            case session = "session"
        }
    }

    struct Account: Codable{
        
        let registered: Bool
        let key: String
        
        enum CodingKeys: String, CodingKey {
            case registered = "registered"
            case key = "key"
        }

    }

    struct Session: Codable {
        
        let expiration: String
        let id: String
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case expiration = "expiration"
        }
    }
}
