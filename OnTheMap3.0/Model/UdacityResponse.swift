//
//  UdacityResponse.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 11/07/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation

struct UdacityResponse: Codable {
    let statusCode: Int
    let statusMessage: String
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}

extension UdacityResponse: LocalizedError {
    var errorDescription: String? {
        return statusMessage
    }
}
