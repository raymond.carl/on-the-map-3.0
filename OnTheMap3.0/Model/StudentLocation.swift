//
//  StudentLocation.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 04/12/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation


struct StudentResponse: Codable{
    let results: [Student]
    
    enum CodingKeys: String, CodingKey {
        case results
    }
}



struct Student: Codable{
    
    let uniqueKey: String
    let firstName: String
    let lastName: String
    let mapString: String
    let mediaURL: String
    let latitude: Double
    let longitude: Double
    
    
    enum CodingKeys: String, CodingKey {
        
        case uniqueKey = "uniqueKey"
        case firstName = "firstName"
        case lastName = "lastName"
        case mapString = "mapString"
        case mediaURL = "mediaURL"
        case latitude = "latitude"
        case longitude = "longitude"
    }
    
}

