//
//  LoginRequest.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 11/07/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation

struct UdacityLoginRequest: Encodable {
    
    let udacity: LoginRequest
    
}

struct LoginRequest: Encodable {
    
    let username: String
    let password: String
    
    /*
    enum CodingKeys: String, CodingKey {
        case username
        case password
    }
 */
}


