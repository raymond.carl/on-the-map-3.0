//
//  UdacityAPI.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 11/07/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation


enum AuthError: Error {
    case BadLogin(Int)
}


// All functionality related to using the Udacity API is done here.
class UdacityAPI{

    
    static var students: Array<Student>?
    
   struct Session {
        static var id = ""
        static var key = ""
    }

    enum Endpoints {
        static let base = "https://onthemap-api.udacity.com/v1"

        case login
        case location

        var stringValue: String {
            switch self {
                case .login:
                    return Endpoints.base + "/session"
                case .location:
                    return Endpoints.base + "/StudentLocation?order=-updatedAt"

            }
        }
        
        var url: URL {
            return URL(string: stringValue)!
        }
        
    }
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func taskForGetRequest<responseType: Decodable>(url: URL, responseType: responseType.Type, completion: @escaping (responseType?,Error?) -> Void) {
        
        let request = URLRequest(url: url)
        let session = URLSession.shared
          
        let task = session.dataTask(with: request) {data, response, error in
          
            if error != nil {
              print("error")
              return
            }
            
            //var dict = convertToDictionary(text: String(data: data!, encoding: .utf8)!)
            do {
                let decoder = JSONDecoder()
                let responseObject = try decoder.decode(responseType.self, from: data!)
                
                completion(responseObject, nil)
                
                // How will cases be handled when there is are no existing registered students and Udacity returns an empty array?
            }
            catch{
                print("Error decoding response from the Udacity API: ", error)
                completion(nil, error)
            }
          }
        
            
            //print(123, cleanData)
        
        
          task.resume()
    }
    
    
    class func taskForPOSTrequest2(body: Student, url: URL){

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpBody = try! JSONEncoder().encode(body)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
          if error != nil { // Handle error…
              return
          }
          print(String(data: data!, encoding: .utf8)!)
        }
        task.resume()
    }

    class func taskForPOSTRequest<RequestType: Encodable, ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, body: RequestType, completion: @escaping (Data?, Error?) -> Void) {
        
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // encoding a JSON body from a string, can also use a Codable struct
        request.httpBody = try! JSONEncoder().encode(body)
        

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            // Why does this need to be done on the main queue?
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            
            print(123, String(data: data, encoding: .utf8)!)
            completion(data, nil)

        }
        task.resume()
    }
    
    class func login(credentials:UdacityLoginRequest, completion: @escaping (Bool, Error?, AuthCredentials?) -> Void){
        
        /*
        var newLocation = Student(uniqueKey: "1234", firstName: "Timmy", lastName: "O'toole", mapString: "Mountain View, CA", mediaURL: "https://udacity.com", latitude: 31.222, longitude: 12.222)
        
        taskForPOSTrequest2(body: newLocation, url: Endpoints.location.url)
        */
        
        
        taskForPOSTRequest(url: Endpoints.login.url, responseType: AuthCredentials.self, body: credentials) {data, error in
            
            let range = 5..<data!.count
            let newData = data!.subdata(in: range) /* subset response data! */
            //print(123, String(data: newData, encoding: .utf8)!)
            
            do {
                    
                    guard let json = try JSONSerialization.jsonObject(with: newData, options: []) as? [String: Any] else {
                        
                        print("Error with response")
                        return
                    }
                    //print(456, json)
                
                    // There must have been a change in the response from the Udacity API because the status no longer appears for successful login attempts. Only appears with 403 on attempts that can't be authenticated.
                    /*
                    guard let status = json["status"] as? Int else {
                        print("Error with response")
                        
                        return
                    }
                    
                    if (status == 403) {
                        print("Bad login attempt")
                        completion(false, AuthError.BadLogin(403), nil)
                        return
                    }
                    */
                
                
                
                    let decoder = JSONDecoder()
                    
            
                    let responseObject = try decoder.decode(AuthCredentials.self, from: newData)
                    
                    DispatchQueue.main.async {
                        completion(true, nil, responseObject)
                    }

                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            
            /*
            if (error != nil) {
                if let error = error {
                    completion(false, error, response)
                }
            }
            
            if let response = response {
                completion(true, nil, response)
            }
            */
        }

    }
    
    class func addLocation(student: Student, completion: @escaping (Bool, Error?, AddLocationResponse?) -> Void) {
        
        var newLocation = Student(uniqueKey: "1234", firstName: "Timmy", lastName: "O'toole", mapString: "Mountain View, CA", mediaURL: "https://udacity.com", latitude: 31.222, longitude: 12.222)
        
        //taskForPOSTrequest2(body: newLocation, url: Endpoints.location.url)
        
        
        taskForPOSTRequest(url: Endpoints.location.url, responseType: AddLocationResponse.self, body: newLocation) {data, error in
            
            do {
                
                /*
                guard let json = try JSONSerialization.jsonObject(with: Data, options: []) as? [String: Any] else {
                    print("Error with Add Location Response")
                    return
                    
                }
                */
                
                let decoder = JSONDecoder()
                let responseObject = try decoder.decode(AddLocationResponse.self, from: data!)
                
                DispatchQueue.main.async {
                    completion(true, nil, responseObject)
                }

            } catch {
                print("Error in the AddLocation function parsing the JSON response.")
            }
            
            
            
        }
        
        
        
    }
    
    // Initiates a GET request to get a list of students and extracts an array of students from the result.
    class func getStudents(completion: @escaping ([Student], Error?) -> Void){
        
        taskForGetRequest(url: Endpoints.location.url, responseType: StudentResponse.self) { response, error in
            if let response = response {
                // Extract the array of students from the results
                completion(Array(arrayLiteral: response.results)[0], nil)
            } else {
                completion([], error)
            }
        }
        
    }
    
    class func loginUser(){
        
        
        
    }

    
}
    


    



