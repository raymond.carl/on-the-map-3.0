//
//  AddLocationResponse.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 10/01/2021.
//  Copyright © 2021 Raymond Carl. All rights reserved.
//

import Foundation


struct AddLocationResponse: Codable {
    
    let objectId: String
    let createdAt: String
    
    enum CodingKeys: String, CodingKey {
        case objectId = "objectId"
        case createdAt = "createdAt"
    }

}
