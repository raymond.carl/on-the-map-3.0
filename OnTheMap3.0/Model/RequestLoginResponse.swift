//
//  RequestTokenResponse.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 11/07/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation

struct AuthCredentials: Codable {
    let account: Account
    let session: Session
    
    enum CodingKeys: String, CodingKey {
        case account = "account"
        case session = "session"
    }
}

struct Account: Codable{
    
    let registered: Bool
    let key: String
    
    enum CodingKeys: String, CodingKey {
        case registered = "registered"
        case key = "key"
    }

}

struct Session: Codable {
    
    let expiration: String
    let id: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case expiration = "expiration"
    }
}


