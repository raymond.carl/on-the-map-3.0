//
//  ErrorMessages.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 03/01/2021.
//  Copyright © 2021 Raymond Carl. All rights reserved.
//

import Foundation

enum Errors {

    case badLogin
    case emptyLocation

    var stringValue: [String:String] {
        switch self {
            case .badLogin:
                return ["title": "Login Error", "message": "Your Email and Password Combination Didn't Work. Please Try Again."]
            case .emptyLocation:
                return ["title": "Missing Information", "message": "Please Add Your Location."]

        }
    }
    
    var title: String {
        return stringValue["title"]!
    }
    
    var message: String {
        return stringValue["message"]!
    }
        
}
