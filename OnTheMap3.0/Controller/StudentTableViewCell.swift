//
//  StudentTableViewCell.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 20/12/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    //MARK: Properties
    
    
    @IBOutlet weak var displayName: UILabel!
    @IBOutlet weak var displayURL: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
