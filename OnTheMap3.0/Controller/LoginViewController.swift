//
//  LoginViewController.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 17/08/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var errorMsg: UILabel!
    static var authSession: AuthCredentials?
    
    @IBAction func signup_button(_ sender: UIButton) {
        // source: https://developer.apple.com/forums/thread/67733
        self.activityIndicator.startAnimating()
        
        
        if let url = NSURL(string: "https://auth.udacity.com/sign-up") {
            UIApplication.shared.open(url as URL, options:[:], completionHandler:nil)
        }
    }
    
    @IBAction func loginButtonTap(_ sender: Any) {
        login()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        errorMsg.isHidden = true
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        passwordField.isSecureTextEntry = true

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func login(){
        self.errorMsg.isHidden = true
        let email:String = emailField.text ?? ""
        let password:String = passwordField.text ?? ""
        
                
        let usernameAndPassword = LoginRequest(username: email, password: password)
        let udacityRequest = UdacityLoginRequest(udacity: usernameAndPassword)
        
        UdacityAPI.login(credentials: udacityRequest, completion: handleLoginResponse(success: error: login_details:))
        
    }
    
    func handleLoginResponse(success: Bool, error: Error?, login_details: AuthCredentials?) -> Void {
        if success {
            LoginViewController.authSession = login_details
            performSegue(withIdentifier: "postLogin", sender: nil)
            //presentViewController("postLogin", animated: true, completion: nil)
        } else {
            if (error != nil) {
                switch (error!) {
                    case AuthError.BadLogin(403):
                      DispatchQueue.main.async { // Correct
                        self.errorMsg.isHidden = false
                      }
                      
                    default:
                        print("Error with login response: ", error)
                        break
                    }
              }
            }
            
        }
    }


