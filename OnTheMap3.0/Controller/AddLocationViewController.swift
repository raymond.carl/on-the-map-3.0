//
//  AddLocationViewController.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 03/01/2021.
//  Copyright © 2021 Raymond Carl. All rights reserved.
//

import UIKit

class AddLocationViewController: UIViewController {
    
    @IBOutlet weak var userLocation: UITextField!
    @IBOutlet weak var userMediaURL: UITextField!
   
    @IBAction func submitButton(_ sender: Any) {
    
    //let email:String = emailField.text ?? ""

        if(userLocation.text == "Your Location (i.e. Atlanta, GA)") {
            
            
            
            let alert = UIAlertController(title: Errors.emptyLocation.title, message: Errors.emptyLocation.message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))

            self.present(alert, animated: true)
        }
        var student = Student(uniqueKey: "1234", firstName: "Timmy", lastName: "O'toole", mapString: "Mountain View, CA", mediaURL: "https://udacity.com", latitude: 31.222, longitude: 12.222)
        
        UdacityAPI.addLocation(student: student, completion: handleAddLocation(success: error: locationResponse:))
        
        
    }
    
    func handleAddLocation(success: Bool, error: Error?, locationResponse: AddLocationResponse?) -> Void{
        print(123, locationResponse)
        performSegue(withIdentifier: "LocationConfirmSegue", sender: nil)
    }
    
    override func viewDidLoad() {
           super.viewDidLoad()

           // Do any additional setup after loading the view.
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
