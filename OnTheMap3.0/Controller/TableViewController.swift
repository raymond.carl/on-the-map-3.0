//
//  TableViewController.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 20/12/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import UIKit


class StuentTableViewController: UITableViewController {
    
    // MARK: Properties
    
    static var studentsArray: Array<Student>?

    override func viewDidLoad() {
        super.viewDidLoad()
       
        //print(235,  UdacityAPI.students!)
         UdacityAPI.getStudents() { students, error in
            StuentTableViewController.studentsArray = students
            print(123, students)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
           
        }
        
        
        
        
        // Use the edit button item provided by the table view controller.
        //navigationItem.leftBarButtonItem = editButtonItem()

        // Load the saved data.
        /*
        if let savedMeals = loadMeals() {
            meals += savedMeals
        }
         */
        
    }
      
    /*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 */

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
       
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return StuentTableViewController.studentsArray?.count ?? 0
    }


  
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // Ask for a cell of the appropriate type.
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell", for: indexPath) as? StudentTableViewCell else {
            fatalError("The dequeued cell is an instance of StudentTableViewCell")
        }
        
        let student:Student
        
        student = StuentTableViewController.studentsArray?[indexPath.row] as! Student
        
       // Configure the cell’s contents with the row and section number.
       // The Basic cell style guarantees a label view is present in textLabel.
        
        let firstName = student.firstName
        let lastName = student.lastName
        let nameConcat = String(firstName as! String)  + " " + String(lastName as! String)
        
        let mediaURL = student.mediaURL
        cell.displayName!.text = nameConcat
        cell.displayURL!.text = String(mediaURL as! String)
        return cell
    }
    
    /*
    class func handleStudentData(data: Array<Student>?, error: Error?) -> Void {
        
        //print (data)
        self.students = data ?? []
        completion()
        
    }
 */
 

}




