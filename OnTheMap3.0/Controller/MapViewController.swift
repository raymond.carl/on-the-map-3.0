//
//  MapViewController.swift
//  OnTheMap3.0
//
//  Created by Raymond Carl on 19/11/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate{
    
    

    @IBOutlet weak var mapView: MKMapView!

    // Not used right now. Delete in final version.
    static var locations: Array<Student>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateMapData()
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseId = "pin"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        
        if pinView == nil {
            
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.pinTintColor = .red
            pinView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
        } else {
            pinView!.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        if control == view.rightCalloutAccessoryView {
            //let app = UIApplication.shared
            if let toOpen = view.annotation?.subtitle! {
                UIApplication.shared.open(URL(string: toOpen)!)
                //app.openURL(URL(string: toOpen)!)
            }
        }
        
    }
    
    func generateMapData(){
        UdacityAPI.getStudents() { students, error in
            
            
            /* MKPointAnnotation Reference: https://developer.apple.com/documentation/mapkit/mkpointannotation
            MKPointAnnotation() is an initiation of a class that creates a point with a basic description.
             Note: never seen this syntax where the parenthesis comes after the array brackets
            */
            
            var annotations = [MKPointAnnotation]()
            MapViewController.locations = students
            
            for student in students {
                let lat = CLLocationDegrees(student.latitude as! Double)
                let long = CLLocationDegrees(student.longitude as! Double)
                
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                
                let first = student.firstName as! String
                let last = student.lastName as! String
                let mediaURL = student.mediaURL as! String
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = coordinate
                annotation.title = "\(first) \(last)"
                annotation.subtitle = mediaURL
                
                annotations.append(annotation)
                
            }
            DispatchQueue.main.async {
                self.mapView.addAnnotations(annotations)
            }
        }
        
        
    }
}
